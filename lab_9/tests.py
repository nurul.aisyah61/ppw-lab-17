from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_session_drones
from .api_enterkomputer import get_drones
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token, get_data_user


#dari lab 7 biar bisa set username dan pass gitu
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')


class Lab9UnitTest(TestCase):
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code,200)

    def test_lab_9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_lab_9_template_using_lab_9_html(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,  "lab_9/session/login.html")
        
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')

    def test_login_wrong_user_pass(self):#line 29
        response = self.client.post('/lab-9/custom_auth/login/', {'username': "utest", 'password': "ptest"})
       # self.assertEqual(response, "Username atau password salah")
        self.client.get('/lab-9/').content.decode('utf-8')

    def test_logout(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/logout/')
        pesan = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", pesan)

    def test_def_profile_in_views(self):
        #sudah terlanjur login langsung aja ke profile kan, kita login icak2 dulu
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

        #tanpa login, kita logout kan dia dulu
        response = self.client.post('/lab-9/custom_auth/logout/')
        pesan = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", pesan)
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_add_drones_di_views(self):
        self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        #add drone
        response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[0]["id"]+'/')
        response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah drone favorite", html_response)

        #delete drone
        response = self.client.post('/lab-9/del_session_drones/'+get_drones().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus dari favorite", html_response)

        #reset drones
        response = self.client.post('/lab-9/clear_session_drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite drones", html_response)

   
        
        
    # COOKIES
    
    def test_profile_cookie_di_views(self):
        #not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_9/cookie/login.html')
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

    def test_auth_login_cookie(self):
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': "utest", 'password': "ptest"})
        self.assertEqual(response.status_code, 302)



    def test_logout_auth_cookie_di_view (self): 
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': "utest", 'password': "ptest"})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/cookie/clear/')
        pesans = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Cookies direset", pesans)

    def test_cookie(self):
        #not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

        #login using HTTP GET method
    def test_login_auth_cookie(self):
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        #login failed, invalid pass and uname


        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'salah', 'password': 'salah'})
        html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Username atau Password Salah", html_response)

        #try to set manual cookies
        self.client.cookies.load({"user_login": "salah", "user_password": "salah"})
        response = self.client.get('/lab-9/cookie/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Kamu tidak punya akses :P ", html_response)

        #login successed
        self.client = Client()
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

        #logout
        response = self.client.post('/lab-9/cookie/clear/')
        html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Cookies direset", html_response)


