from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, mhs_name, calculate_age
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here.

class Lab1UnitTest(TestCase):

    def test_hello_name_is_exist(self):
        response = Client().get('/lab-1/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-1/')
        self.assertEqual(found.func, index)

    def test_name_is_changed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<h1>Hello my name is ' + mhs_name + '</h1>', html_response)
        self.assertFalse(len(mhs_name) == 0)


    def test_calculate_age_is_correct(self):
        curr_year = date.today().year
        self.assertEqual(0, calculate_age(date.today().year))
        test1 = curr_year - 2000 if 2000 <= curr_year else 0
        
        self.assertEqual(test1, calculate_age(2000))
        


    def test_index_contains_age(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'<article>I am [0-9]\d+ years old</article>')
