from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, PageNotAnInteger
from django.forms.models import model_to_dict

from .models import Friend, Mahasiswa
from api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    for mhs in mahasiswa_list:
        mahasiswa = Mahasiswa(mahasiswa_name = mhs['nama'], mahasiswa_npm = mhs['npm'])
        mahasiswa.save()

    paginator = Paginator(mahasiswa_list, 25)
    list_mahasiswa = Mahasiswa.objects.all()
    page = request.GET.get('page')

    try:
        halaman = paginator.page(page)
    except PageNotAnInteger:
        halaman = paginator.page(1)

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": halaman, 
                "friend_list": friend_list, 
                "author" : "Nurul Aisyah",
                }
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar-teman.html'
    return render(request, html, response)

def friend_list_json(request):
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results":friends}, content_type = "application/json")

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        valid = isNpmValid(npm)
        data = {'validation' : valid}
        if data['validation'] == True:
            friend = Friend(friend_name = name, npm = npm)
            friend.save()
            data['friends'] = model_to_dict(friend)
        return JsonResponse(data)
        # try:
        #     db_data = Friend.objects.get(npm=npm)
        #     return HttpResponseBadRequest()
        # except Friend.DoesNotExist:
        #     friend = Friend(friend_name=name, npm=npm)
        #     friend.save()
        #     data = model_to_dict(friend)
        #     return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id = friend_id).delete()
        response = {'id' : friend_id}
        return JsonResponse(response)

@csrf_exempt
def validate_npm(npm):
    data = {'is_taken' : not isNpmValid(npm)}
    return JsonResponse(data)

def isNpmValid(npm):
    friends = Friend.objects.all()
    for f in friends:
        if f.npm ==npm :
            return False;
    return True;
#def delete_friend(request, friend_id):
    # Friend.objects.filter(id=friend_id).delete()
    # return HttpResponseRedirect('/lab-7/get-friend-list')


# @csrf_exempt
# def validate_npm(request):
   
#     npm = request.POST.get('npm', None)
#     try:
#         db_friend = Friend.objects.get(npm=npm) 
#         is_taken = True 
#     except Friend.DoesNotExist:
#         is_taken = False
     
#     data = {
#         'is_taken': is_taken,#lakukan pengecekan apakah Friend dgn npm tsb sudah ada
#     }
#     return JsonResponse(data)


# def model_to_dict(obj):
#     data = serializers.serialize('json', [obj,])
#     struct = json.loads(data)
#     data = json.dumps(struct[0]["fields"])
#     return data


# def get_friend_data(request):
#     data = serializers.serialize('json', Friend.objects.all())
#     struct = json.loads(data)

#     data = []
#     for i in range(len(struct)):
#         data.append(eval)