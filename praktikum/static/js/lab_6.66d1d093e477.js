//chatbox

//appendChat
function enterChatBox(event) {
    var notEmpty = $("#isiChat").val() != "";
    var rply = ["<b>admin: </b> Iya",
                "<b>admin: </b> Tidak",
                "<b>admin: </b> Nggak",
                "<b>admin: </b> Bisa jadi",
                "<b>admin: </b> Menarik",
                "<b>admin: </b> Gak tau",
                ]

    if(e.which == 13) {
    var txt = "<b>me: </b>"+$("#isiChat").val();
    var min = 0;
    var max = 5;
    var random = Math.floor(Math.random() * (max - min + 1)) + min;
    if(notEmpty){
      $('<p>'+txt+'</p>').addClass('msg-send').appendTo('.msg-insert');
      $("#isiChat").val("");
      $('<p>'+rply[random]+'</p>').addClass('msg-receive').appendTo('.msg-insert');
      $(".chat-body").scrollTop($(".chat-body").height());
    }else{
      alert("Message Empty");
      }
    }
}

//reset textarea
function reset(e){
  if (e.which == 13) {
      $("#isiChat").val("");
  }
}
$("#isiChat").keypress(appendChat);

$("#isiChat").keyup(reset);
//END Chat Box




// Calculator
var print = document.getElementById('print');
var erase = false;
Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};
Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = null;
    erase =true;
  }else if(x === 'log'){
    print.value =  Math.log10(Math.round(evil(print.value) * 10000 / 10000)).toFixed(10);
    erase = true;  
  }else if(x === 'sin' || x === 'tan'){
    print.value = Math.round(evil('Math.'+x+'(Math.radians('+evil(print.value)+'))') * 10000) / 10000;
    erase = true;
  }else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  }else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//change theme

var themes;
$(document).ready(function() {

    themes = 
    [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

    localStorage.setItem("themes", JSON.stringify(themes));


    mySelect = $('.my-select').select2();

    mySelect.select2({
    'data': JSON.parse(localStorage.getItem("themes"))
  })

});

var themesData = JSON.parse(localStorage.getItem("themes"));
var selectedTheme = themesData[3];

if(localStorage.getItem("selectedTheme") !== null){
  selectedTheme = JSON.parse(localStorage.getItem("selectedTheme"));
}
$('body').css(
{
  "background-color": selectedTheme.bcgColor,
  "font-color": selectedTheme.fontColor
});

$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
     var idChoose = mySelect.val();


    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    if(idChoose >= 0 && idChoose <= 10){
      // [TODO] ambil object theme yang dipilih
      selectedTheme = themesData[idChoose];
    }
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    $('body').css(
      {
        "background-color": selectedTheme.bcgColor,
         "font-color": selectedTheme.fontColor
      
      }
    );
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem("selectedTheme",JSON.stringify(selectedTheme));
});
